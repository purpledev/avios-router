// contentful stuuff

var client = contentful.createClient({
  space: 'qfl31ojvdm20',
  accessToken: '09b6897079b1433d983ffd75f4c4aba997e1dda9454be21c08b032c99e3854cc'
})


 
  
function blogListingFeed(type) {
    client.getEntries({
      'content_type': 'blog',
//	  order: '-fields.publishTime'
      //'fields.type.sys.contentType.sys.id': 'layoutFeatured',
      //'items.fields.modules.fields': 'layout'
      //'fields.type.fields.title[match]': 'Featured',
    }).then(function (entries) {  
      entries.items.forEach(function (entry) {	
		var rowBlog = "";		  
		var d = new Date(entry.fields.publishTime);
		
		var blog_year = d.getFullYear();
		var blog_date = get_date(d);
		var blog_time = get_time(d);		 

		 rowBlog +=('<div class="blogrow rowFlex"><div class="one-half column bgImage" style="background-image: url(' +entry.fields.heroImage.fields.file.url+ ');"></div>' +
                               ' <div class="one-half column whiteBG"><div class="blogrowContent">'+
                                '<div class="heading"><h4>'+entry.fields.title+'</h4></div>' +
                               '<div class="date"><p>'+blog_date+' | '+blog_year+'</p></div>' +
                                ' <p>'+entry.fields.description+'</p>' +
                                ' <a href="/#/blog/'+entry.fields.slug+'" data-ref="' + entry.sys.id + '" id="blogLink" class="button readmore">Read more</a>' +
                               '</div></div></div>');		
							   
							   
		var checkExist = setInterval(function() {
			if ($('#BlogListing').length) {
			 $('#BlogListing .listing').append(rowBlog);
			 //$('#BlogListing .listing').append('<div class="clear"></div>');  
			 clearInterval(checkExist);
                loadMore()
			}
		}, 100);					   
	  })	  
	 })
} 


function blogArticle(slug) {
 	
	 client.getEntries({
      'content_type': 'blog',
	  'fields.slug': slug
      //'fields.type.sys.contentType.sys.id': 'layoutFeatured',
      //'items.fields.modules.fields': 'layout'
      //'fields.type.fields.title[match]': 'Featured',
    }).then(function (entries) {           
      

	   entries.items.forEach(function (entry) {	
	       console.log(entry);
			var d = new Date(entry.fields.publishTime);
			var blog_year = d.getFullYear();
			var blog_date = get_date(d);
			var blog_time = get_time(d);		 
			var blog_title = entry.fields.title;
			var blog_hero = entry.fields.heroImage.fields.file.url;
			//var blog_content = entry.fields.blogContent.fields.body;
			
			var blog_body = entry.fields.blogBody;
			
			var blog_author = entry.fields.authorName;
			var blog_author_jt = entry.fields.authorJobTitle;
            var blog_author_img = entry.fields.authorImage.fields.file.url;
	  
		    if (entry.fields.related) { 
                var related_hero = entry.fields.related.fields.heroImage.fields.file.url;
                var related_title = entry.fields.related.fields.title;
                var related_year = d.getFullYear();
                var related_description = entry.fields.related.fields.description;
                var related_slug = entry.fields.related.fields.slug;
            } else {
                
            }
           
            
           // Related blog
           
            var checkExist = setInterval(function() {
                if ($('.blogArticle').length) {
                    $('#app .heroSectionNoHeading').css('background-image', 'url(' + blog_hero + ')');
                    $('.blogArticle .blogArticleInfo .heading h4').html(blog_title);
                    $('.blogArticle .blogArticleInfo .date p').html(blog_date + ' | ' + blog_year);
					
					
                    $('.blogArticle .blogArticleContent .blogContent').html((marked(blog_body)));
                    $('.blogArticle .author .authorDetails .name').html(blog_author);
                    $('.blogArticle .author .authorDetails .title').html(blog_author_jt);
                    $('.blogArticle .author .authorImage img').attr('src', blog_author_img);
                    
                    // Add related post
                  if (entry.fields.related) { 
                    $('#relatedBlogListing').show();
                    $('#relatedBlogListing .blogrow').html('<div class="one-half column bgImage" style="background-image: url(' + related_hero + ');"></div><div class="one-half column whiteBG"><div class="blogrowContent"><div class="heading"><h4>' + related_title + '</h4></div><div class="date"><p>' + blog_date + ' | ' + related_year + '</p></div><p>' + related_description + '</p><a href="/#/blog/' + related_slug + '" class="button readmore">Read more</a></div></div>');
                  } else {
                      $('#relatedBlogListing').hide();
                  }
                    
                    
                   clearInterval(checkExist);
                }
            }, 100);
			
	 })
	})
	
}


function get_date(d){
			var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");	
			var curr_date = d.getDate();
			var sup = "";
			if (curr_date == 1 || curr_date == 21 || curr_date ==31)
			{
			sup = "st";
			}
			else if (curr_date == 2 || curr_date == 22)
			{
			sup = "nd";
			}
			else if (curr_date == 3 || curr_date == 23)
			{
			sup = "rd";
			}
			else
			{
			sup = "th";
			}

			var curr_month = d.getMonth();
			
			return (curr_date + sup + " " + m_names[curr_month]);
}
                        
                        
  function get_time(d){	  
		var curr_hour = d.getHours();			
		if (curr_hour < 12)
		{
		a_p = "am";
		}
		else
		{
		a_p = "pm";
		}
		if (curr_hour == 0)
		{
		curr_hour = 12;
		}
		if (curr_hour > 12)
		{
		curr_hour = curr_hour - 12;
		}
		var curr_min = d.getMinutes();
				
		curr_min = curr_min + "";

		if (curr_min.length == 1)
		{
		curr_min = "0" + curr_min;
		}
		return curr_hour + ":" + curr_min + a_p; 
  }  
                       
                    




 
function homePageFeed(type) {
    client.getEntries({
      'content_type': 'layoutFeatured',
      
    }).then(function (entries) {     
      // log the title for all the entries that have it
        var rowDownloads = "";
       //console.log(entries)
        entries.includes.Entry.forEach(function (entry) {
            //console.log(entry)
            if(entry) {
                
                
                
                var title = entry.fields.title;
                var description = entry.fields.description;
                var image = entry.fields.resource.fields.file.url;
                var url = entry.fields.resource.fields.file.url;
                
                if  (entry.fields.downloadFile) {
                   var downloadURL = entry.fields.downloadFile.fields.file.url;
                } else {
                   var downloadURL = '#';
                }
                
                
                
                
                rowDownloads+=('<div class="two columns pdfDownload">' +
                               '<img src="' + image + '" class="scale-with-grid" alt="Turn everyday spending into a customer delight" />' +
                               '<div class="title">'+
                                '<h6>' + title + '</h6>' +
                               '</div>' +
                               '<a href="' + downloadURL + '" target="_blank">Download PDF</a>' +
                               '</div>');

           }
            var checkExist = setInterval(function() {
                if ($('#featuredContentList').length) {
                    $('#featuredContentList').html(rowDownloads);
                    $('#featuredContentList').append('<div class="clear"></div>');
                    $('#featuredContentList .pdfDownload').hide();
                    $('#featuredContentList .pdfDownload').slice(0, 6).show();
                    clearInterval(checkExist);
                }
            }, 100);
        })

		
     
	})
} 
 
//homePageFeed() 

function homeBlogArticle() {
    client.getEntries({
      'content_type': 'blog',
	  order: '-fields.publishTime',
      limit: 1
      //'fields.type.sys.contentType.sys.id': 'layoutFeatured',
      //'items.fields.modules.fields': 'layout'
      //'fields.type.fields.title[match]': 'Featured',
    }).then(function (entries) {           
      

      entries.items.forEach(function (entry) {
		var rowBlog = "";		  
		var d = new Date(entry.fields.publishTime);
		
		var blog_year = d.getFullYear();
		var blog_date = get_date(d);
		var blog_time = get_time(d);		 

		 rowBlog +=('<div class="blogrow rowFlex"><div class="one-half column bgImage" style="background-image: url('+entry.fields.heroImage.fields.file.url+');"></div>' +
                               ' <div class="one-half column paleBlueBG"><div class="blogrowContent">'+
                                '<div class="heading"><h4>'+entry.fields.title+'</h4></div>' +
                               '<div class="date"><p>'+ blog_date + ' | '+blog_year+'</p></div>' +
                                ' <p>'+entry.fields.description+'</p>' +
                                ' <a href="/#/blog/'+entry.fields.slug+'" data-ref="'+entry.sys.id+'" id="blogLink" class="button readmore">Read more</a>' +
                               '</div></div></div>');		
							   
							   
		var checkExist = setInterval(function() {
			if ($('#homeBlogListing').length) {
			 $('#homeBlogListing .homeBlogItem').append(rowBlog);
			 //$('#BlogListing .listing').append('<div class="clear"></div>');  
			 clearInterval(checkExist);
			}
		}, 100);					   
	  })	  
	 })
}


// Home page slider content
function homeCarouselContent() {
    client.getEntries({
      'content_type': 'layoutCarousel',
    }).then(function (entries) {       
      
       entries.includes.Entry.forEach(function (entry) {
           console.log(entry);
            var carouselContent = "";		  
            var brochureName = entry.fields.title;
            //var resourceType = entry.fields.type.fields.title;
            var description = entry.fields.description;
            //var contentUrl = entry.fields.resource.fields.file.url
            if  (entry.fields.downloadFile) {
               var contentUrl = entry.fields.downloadFile.fields.file.url;
            } else {
               var contentUrl = '#';
            }


             carouselContent +=('<div class="brochureCarouselContent transWhiteBG text-align-center ">' +
                                '<h4>' + description + '</h4>' +
                                '<h6>' + brochureName + '</h6>' +
                                '<a href="' + contentUrl + '" class="button" target="_blank">View</a>' +
                                '</div>');		
							   
							   
		var checkExist = setInterval(function() {
			if ($('.brochureCarousel').length) {
			 $('.brochureCarousel').append(carouselContent);
			 //$('#BlogListing .listing').append('<div class="clear"></div>');  
			 clearInterval(checkExist);
			}
            
		}, 100);					   
	  })
        brochureCarousel();
	 })
}


function videoFeed() {
    client.getEntries({
      'content_type': 'resource',
      'fields.type.sys.contentType.sys.id': 'category',
      'fields.type.fields.title[match]': 'Video',
    }).then(function (entries) { 
        
        //console.log('videos');
      // log the title for all the entries that have it
        var rowDownloads = "";
        entries.items.forEach(function (entry) {
            
            if(entry.fields.title) {
               
                if  (entry.fields.downloadFile) {
                   var downloadURL = entry.fields.downloadFile.fields.file.url;
                } else {
                   var downloadURL = '#';
                }
                
                var placeholderUrl = entry.fields.resource.fields.file.url;
                
                //var videoUrl = entry.fields.downloadFile.fields.file.url

                rowDownloads+=('<div class="four columns listingItem" data-category="' + entry.fields.sector.fields.slug + '">' +
                               '<div class="title">'+
                                '<h6>' + entry.fields.title + '</h6>' +
                               '</div>' +
                                '<video width="100%" height="auto" controls poster="' + placeholderUrl + '"><source src="' + downloadURL + '" type="video/mp4">Your browser does not support the video tag.</video>' +
                               '<a href="' + downloadURL + '" target="_blank">Watch</a>' +
                               '</div>');

            }
        })
		
	var checkExist = setInterval(function() {
			if ($('#videoListing').length) {
			  $('#videoListing .listing').html(rowDownloads);
			  $('#videoListing .listing').append('<div class="clear"></div>');
			  clearInterval(checkExist);
              $('#videoListing a').click(function(e){
                  e.preventDefault();
                  $(this).prev('video').trigger('play');
              });
			}
		}, 100);				
		
    
    })
}



function brochureFeed() {
    client.getEntries({
      'content_type': 'resource',
      'fields.type.sys.contentType.sys.id': 'category',
      'fields.type.fields.title[match]': 'Whitepaper'
    }).then(function (entries) { 
      // log the title for all the entries that have it
        var rowDownloads = "";
        entries.items.forEach(function (entry) {
            
            
            var slug = entry.fields.sector.fields.slug;
            var title = entry.fields.title;
            var imgUrl = entry.fields.resource.fields.file.url;
            var description = entry.fields.description;
            var pdfLink = entry.fields.downloadFile.fields.file.url;
//            if(entry.fields.title) {
                
//                if  (entry.fields.downloadFile) {
//                   var downloadURL = entry.fields.downloadFile.fields.file.url;
//                } else {
//                   var downloadURL = '#';
//                }

                rowDownloads+=('<div class="two columns listingItem" data-category="' + slug + '">' +
                               '<div class="title">'+
                                '<img src="' + imgUrl + '" class="scale-with-grid" alt="Turn everyday spending into a customer delight" />' +
                                '<h6>' + title + '</h6>' +
                               '</div>' +
                               '<a href="' + pdfLink + '" target="_blank">Download PDF</a>' +
                               '</div>');

            
        })
		
		var checkExist = setInterval(function() {
			if ($('#brochuresWhitepapers').length) {
			    $('#brochuresWhitepapers .listing').html(rowDownloads);
				$('#brochuresWhitepapers .listing').append('<div class="clear"></div>');
			   clearInterval(checkExist);
			}
		}, 100);		
    
    })
}



function overviewsheetFeed() {
    client.getEntries({
      'content_type': 'resource',
      'fields.type.sys.contentType.sys.id': 'category',
      'fields.type.fields.title[match]': 'Overview Sheets'
    }).then(function (entries) { 
      // log the title for all the entries that have it
        var rowDownloads = "";
        entries.items.forEach(function (entry) {
            if(entry.fields.title) {
                //console.log(entry.fields.sector.fields.slug)
                //console.log(entry.fields.title)
                //console.log(entry.fields.description)
                //console.log(entry.fields.sector.fields.slug)
                if  (entry.fields.downloadFile) {
                   var downloadURL = entry.fields.downloadFile.fields.file.url;
                } else {
                   var downloadURL = '#';
                }

                rowDownloads+=('<div class="two columns listingItem" data-category="' + entry.fields.sector.fields.slug + '">' +
                               '<img src="' + entry.fields.resource.fields.file.url + '" class="scale-with-grid" alt="Turn everyday spending into a customer delight" />' +
                               '<div class="title">'+
                                '<h6>' + entry.fields.title + '</h6>' +
                               '</div>' +
                                
                               '<a href="' + downloadURL + '" target="_blank">Download PDF</a>' +
                               '</div>');

            }
        })
		
		var checkExist = setInterval(function() {
			if ($('#overviewSheets').length) {
			     $('#overviewSheets .listing').html(rowDownloads);
				$('#overviewSheets .listing').append('<div class="clear"></div>');
			clearInterval(checkExist);
			}
		}, 100);			
		
   
    })
}



function caseStudyFeed() {
    client.getEntries({
      'content_type': 'resource',
      'fields.type.sys.contentType.sys.id': 'category',
      'fields.type.fields.title[match]': 'Case Studies'
    }).then(function (entries) { 
      // log the title for all the entries that have it
        var rowDownloads = "";
        entries.items.forEach(function (entry) {
            //console.log(entry);
            if(entry.fields.title) {
                
                if  (entry.fields.downloadFile) {
                   var downloadURL = entry.fields.downloadFile.fields.file.url;
                } else {
                   var downloadURL = '#';
                }

                rowDownloads+=('<div class="two columns listingItem" data-category="' + entry.fields.sector.fields.slug + '">' +
                               '<img src="' + entry.fields.resource.fields.file.url + '" class="scale-with-grid" alt="Turn everyday spending into a customer delight" />' +
                               '<div class="title">'+
                                '<h6>' + entry.fields.title + '</h6>' +
                               '</div>' +
                               '<a href="' + downloadURL + '">Download PDF</a>' +
                               '</div>');

            }
        })
		
			var checkExist = setInterval(function() {
			if ($('#caseStudies').length) {
			    $('#caseStudies .listing').html(rowDownloads);
				$('#caseStudies .listing').append('<div class="clear"></div>');
			clearInterval(checkExist);
			}
		}, 100);		
    
    })
}




// Menu Highlight
    
function menuAnimation(){
    if ($(window).width() > 767) {
        $('#nav-icon').click(function(){
            $(this).toggleClass('open');
            $('.hiddenNav').fadeToggle();
            $('body').toggleClass('open');
        });
        $('.hiddenNav .links .nav ul li a, .header .logo a').click(function(){
            $('#nav-icon').removeClass('open');
            $('.hiddenNav').fadeOut();
            $('body').removeClass('open');
        });
        
    } else {
        $('#nav-icon').click(function(){
            $(this).toggleClass('open');
            $('body').toggleClass('open');
            $('.site-container').toggleClass('open');
        });
        $('.hiddenNav .links .nav ul li a, .header .logo a').click(function(){
            $('#nav-icon').removeClass('open');
            $('body').removeClass('open');
            $('.site-container').removeClass('open');
        });
    }
    
}

// Image scale on scroll

function imageScale() {
    if ($(window).width() > 1024) {
        $(".heroSection .backgroundThree").css("background-size", (100 + 90 * $(window).scrollTop() / 550) + "%")
        $(".heroSection .backgroundTwo").css("background-size", (100 + 50 * $(window).scrollTop() / 550) + "%");
    } else {
        
    }
}

// Change styling of header on scroll

function headerChange() {   
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $('.header').addClass('scrolled');
    } else {
        $('.header').removeClass('scrolled');
    }
}

//Parallax Scrolling

function runStallar() {
    var checkExist = setInterval(function() {
        if ($('.heroSectionChild').length) {
            $.stellar({
                horizontalScrolling: false,
                responsive: true,
                verticalOffset: 0
            });
        clearInterval(checkExist);
        }
    }, 100);
    
}

// Carousel 

function brochureCarousel() {
    var checkExist = setInterval(function() {
        if ($('.brochureCarousel').length) {
            $('.brochureCarousel').slick({
             dots: true,
             infinite: true,
             fade: false,
             autoplay: false,
             speed: 1000,
             responsive: true,
             nextArrow: document.getElementById('slick-next'),
             prevArrow: document.getElementById('slick-previous')
           });
        clearInterval(checkExist);
        }
    }, 100);
}

// Blog post 

function loadMore() {
    var checkExist = setInterval(function() {
        if ($('#BlogListing .blogrow').length) {
            $('#BlogListing .blogrow').hide();
            $('#BlogListing .blogrow').slice(0, 4).show();
            if ($('#BlogListing .blogrow').length <= 4 ) {
                $('.viewmore').hide();
            } else {
               $('.viewmore').show(); 
            }
            $('.viewmore').on('click', function (e) {
                e.preventDefault();
                $('#BlogListing .blogrow:hidden').slice(0, 1).fadeIn();
                if ($('#BlogListing .blogrow:hidden').length == 0) {
                    $('.viewmore').fadeOut('slow');
                }
            });
        clearInterval(checkExist);
        }
    }, 100);
    
}

//Filter News


function filterContentOne() {
    var checkExist = setInterval(function() {
        if ($('#downloadListing').length) {
           $('.filterRow a').click(function(e) {
                //console.log('test');
                e.preventDefault();
                var filter = $(this).attr('id');
                filterList(filter);
                $('.filterRow a').removeClass('active');
                $(this).addClass('active');
            });
        clearInterval(checkExist);
        }
    }, 100);
    
}


function filterContent(){
    
    var checkExist = setInterval(function() {
        if ($('#downloadListing').length) {
           $('.rowInternalLinks ul li a').click(function(e){
                console.log('clicked');
                e.preventDefault();
                var divId = $(this).attr('href');
                $(divId).fadeToggle();
                $(this).toggleClass('active');
            });
        clearInterval(checkExist);
        }
    }, 100);
    
}

// News filter function

function filterList(value) {
	var list = $('.rowListing .listing .listingItem');
	$(list).fadeOut('fast');
	if (value === 'All') {
		$('#downloadListing .rowListing .listing .listingItem').fadeIn();
	} if (value === 'All active') {
		$('#downloadListing .rowListing .listing .listingItem').fadeIn();
	} else {
		$('.rowListing .listing').find(".listingItem[data-category*=" + value + "]").each(function (i) {
			$(this).fadeIn();
		});
	}
}

// Add filters for mobile 

function mobileFilterChoices() {
    var checkExist = setInterval(function() {
        if ($('.filterContent').length) {
            $('.filterButton').click(function(e){
                //alert('clicked');
                e.preventDefault();
                $('#nav-icon').hide();
                $('body').toggleClass('open');
                $('.filterContainer').fadeIn();
                $('.closeButton').click(function(){
                    $('.filterContainer').fadeOut();
                    $('body').removeClass('open');
                    $('#nav-icon').show();
                });
            });
            $('.filterButtonClose').click(function(e){
                e.preventDefault();
                $('#nav-icon').show();
                $('body').removeClass('open');
                $('.filterContainer').fadeOut();
            });
        clearInterval(checkExist);
        }
    }, 100);
    
}

// Add rows to listing

function addClass() {
    $('#videoListing .listing .listingItem').each( function (index) {
      index += 1;
      if(index % 5 == 0) {
        $(this).addClass('alpha');
      }
    });
    $('#brochuresWhitepapers .listing .listingItem').each( function (index) {
      index += 1;
      if(index % 7 == 0) {
        $(this).addClass('alpha');
      }
    });
    $('#overviewSheets .listing .listingItem').each( function (index) {
      index += 1;
      if(index % 7 == 0) {
        $(this).addClass('alpha');
      }
    });
    $('#caseStudies .listing .listingItem').each( function (index) {
      index += 1;
      if(index % 7 == 0) {
        $(this).addClass('alpha');
      }
    });
}

// Smooth scroll

function smoothScroll() {
    var headerHeight = $('.header').outerHeight();
    $('.internalLink').click(function(event){
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - headerHeight
        }, 500);
        event.preventDefault();
    });
}

function reloadPage(){
    window.location.href = "http://localhost:3000/";
}

function addCurrentYear() {
    $('.footer-copy .year').html(new Date().getFullYear());
}

function scrollToVideo() {
    $("html, body").animate({ scrollTop: 0 });
    var checkExist = setInterval(function() {
            $('html, body').animate({
            scrollTop: $('#videoListing').offset().top - $('.header').outerHeight()
        }, 500, 'linear');
        clearInterval(checkExist);
       
    }, 500);
}

function scrollToBrochures() {
    $("html, body").animate({ scrollTop: 0 });
    var checkExist = setInterval(function() {
        
        $('html, body').animate({
            scrollTop: $('#brochuresWhitepapers').offset().top - $('.header').outerHeight()
        }, 500, 'linear');
        clearInterval(checkExist);
    }, 500);
}

function scrollToCaseStudies() {
    $("html, body").animate({ scrollTop: 0 });
    var checkExist = setInterval(function() {
        
        $('html, body').animate({
            scrollTop: $('#caseStudies').offset().top - $('.header').outerHeight()
        }, 500, 'linear');
        clearInterval(checkExist);
    }, 500);
}


$(document).ready(function(){
    addClass()
    //loadMore()
    menuAnimation()
    addCurrentYear()
})
 
$(window).scroll(function(){
    headerChange()
    imageScale()   
});

//$(window).unload(function(){
//
//     reloadPage()
//
//});

