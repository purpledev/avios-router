(function($) {

//function routing() {

  var app = $.sammy('#app', function() {
    
    this.use('Template');

    this.around(function(callback) {
      var context = this;
      this.load('/data/articles.json')
          .then(function(items) {
            context.items = items;
          })
          .then(callback);
    });

	 this.get('/#/blog/', function(context) {
		
        context.app.swap('');
        context.render('/templates/blog.template', {}).appendTo(context.$element());			   
		$("html, body").animate({ scrollTop: 0 });	
        
		blogListingFeed()
        
    });

	 this.get('/#/resources/', function(context) {
		
		
        var str=location.href.toLowerCase();
        context.app.swap('');
        context.render('/templates/resources.template', {})
               .appendTo(context.$element());
		$("html, body").animate({ scrollTop: 0 });	
         var pageUrl = window.location.href;
        console.log(pageUrl);
		videoFeed()
		brochureFeed()
		overviewsheetFeed()
		caseStudyFeed()	   
		mobileFilterChoices()
        filterContent()
        filterContentOne()
        
    });	
    this.get('/resources/#videoListing', function(context) {
		
		
        var str=location.href.toLowerCase();
        context.app.swap('');
        context.render('/templates/resources.template', {})
               .appendTo(context.$element());
         
		videoFeed()
		brochureFeed()
		overviewsheetFeed()
		caseStudyFeed()	   
		mobileFilterChoices()
        filterContent()
        filterContentOne()
        
        scrollToVideo();
    });
    this.get('/resources/#brochuresWhitepapers', function(context) {
		
		
        var str=location.href.toLowerCase();
        context.app.swap('');
        context.render('/templates/resources.template', {})
               .appendTo(context.$element());
         
		videoFeed()
		brochureFeed()
		overviewsheetFeed()
		caseStudyFeed()	   
		mobileFilterChoices()
        filterContent()
        filterContentOne()
        
        scrollToBrochures();
    });
    this.get('/resources/#caseStudies', function(context) {
		
		
        var str=location.href.toLowerCase();
        context.app.swap('');
        context.render('/templates/resources.template', {})
               .appendTo(context.$element());
         
		videoFeed()
		brochureFeed()
		overviewsheetFeed()
		caseStudyFeed()	   
		mobileFilterChoices()
        filterContent()
        filterContentOne()
        
        scrollToCaseStudies();
    });
	
    this.get('/#/', function(context) {		
	   
      context.app.swap('');
       context.render('/templates/home.template',{}	)
               .appendTo(context.$element());
	  $("html, body").animate({ scrollTop: 0 });
      
      //runStallar()
	  homePageFeed() 	
      homeCarouselContent()
	  //brochureCarousel()
      homeBlogArticle()
      
    });
      
//    this.bind('mycustom-trigger', function (e, data) {
//        this.redirect('/'); // force redirect
//    });
    

    this.get('/#/blog/:id', function(context) {
		
		
		
	 // console.log(this.params['id']);		
		
		
      
		var pageUrl = window.location.href;
        console.log(pageUrl);
		/*
      this.item = this.items[this.params['id']];
      if (!this.item) { return this.notFound(); }*/
      this.partial('/templates/blog-detail.template');
      $("html, body").animate({ scrollTop: 0 });	
	  blogArticle(this.params['id'])
      
	  
	  
	  
    });
      
      

/*
    this.before('.*', function() {

	console.log('hee45');
	
        var hash = document.location.hash;
        $("nav").find("a").removeClass("current");
        $("nav").find("a[href='"+hash+"']").addClass("current");
   });
*/
  });

  $(function() {
	  
	  app.run('/#/');
	
  });
    
 

//}
})(jQuery);














